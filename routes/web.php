<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/logout', [App\Http\Controllers\Auth\AuthenticatedSessionController::class, 'logout'])->name('logout');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
//*********************************** USERMANAGEMENT ***********************************************

Route::get('user/management', [App\Http\Controllers\UserManagementController::class, 'index'])->middleware(['auth'])->name('users');
Route::get('edit_user/{id}', [App\Http\Controllers\UserManagementController::class, 'edit_user'])->middleware('auth');
Route::post('update_user', [App\Http\Controllers\UserManagementController::class, 'update_user'])->name('update_user');
Route::get('delete_user/{id}', [App\Http\Controllers\UserManagementController::class, 'delete'])->middleware('auth');




//********************************* SERVICES***********************************************/
Route::get('services/service', [App\Http\Controllers\ServiceController::class, 'index'])->middleware(['auth'])->name('servive');
Route::get('service/add/new', [App\Http\Controllers\ServiceController::class, 'addNew'])->name('service/add/new');
Route::post('service/add/save', [App\Http\Controllers\ServiceController::class, 'addNewServiceSave'])->name('service/add/save');
Route::get('edit_service/{id}', [App\Http\Controllers\ServiceController::class, 'edit_service'])->middleware('auth');
Route::post('update', [App\Http\Controllers\ServiceController::class, 'update'])->name('update');
Route::get('delete_service/{id}', [App\Http\Controllers\ServiceController::class, 'delete'])->middleware('auth');


/**********************************APPAREILS*****************************************************/
Route::get('appareils/appareil', [App\Http\Controllers\AppareilController::class, 'index'])->middleware(['auth'])->name('appareil');
Route::get('appareil/add/new', [App\Http\Controllers\AppareilController::class, 'addNew'])->name('appareil/add/new');
Route::post('apareils/add/save', [App\Http\Controllers\AppareilController::class, 'addNewAppareilSave'])->name('appareils/add/save');
Route::get('edit_appareil/{id}', [App\Http\Controllers\AppareilController::class, 'edit_appareil'])->middleware('auth');
Route::post('update_appareil', [App\Http\Controllers\AppareilController::class, 'update_appareil'])->name('update_appareil');
Route::get('delete_app/{id}', [App\Http\Controllers\AppareilController::class, 'delete'])->middleware('auth');


/**********************************TACHES*****************************************************/
Route::get('taches/tache', [App\Http\Controllers\TacheController::class, 'index'])->middleware(['auth'])->name('tache');
Route::get('tache/add/new', [App\Http\Controllers\TacheController::class, 'addNew'])->name('tache/add/new');
Route::post('taches/add/save', [App\Http\Controllers\TacheController::class, 'addNewTacheSave'])->name('taches/add/save');
Route::get('edit_tache/{id}', [App\Http\Controllers\TacheController::class, 'edit_tache'])->middleware('auth');
Route::post('update_tache', [App\Http\Controllers\TacheController::class, 'update_tache'])->name('update_tache');
Route::get('delete_app/{id}', [App\Http\Controllers\TacheController::class, 'delete'])->middleware('auth');

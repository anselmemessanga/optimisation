<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\User;
use Hash;
use Brian2694\Toastr\Facades\Toastr;


class UserManagementController extends Controller
{
    // index
    public function index()
    {
        $user = DB::table('users')->get();
        return view('users.userManagement',compact('user'));
    }

    // update
public function edit_user($id)
{

    $data = DB::table('users')->where('id',$id)->get();
    return view('users.edituser',compact('data'));
}

public function update_user(Request $request)
{
    $id           = $request->id;
    $name     = $request->name;
    $email     = $request->email;

    $update = [

        'id'     => $id,
        'name'   => $name,
        'email'  => $email,
    ];
    User::where('id',$request->id)->update($update);
    return redirect('user/management');
}

    // delete
   public function delete($id)
   {
       $delete = User::find($id);
       $delete->delete();
       //Toastr::success('User deleted successfully :)','Success');
       return redirect('user/management');
   }
}

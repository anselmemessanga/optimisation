<?php

namespace App\Http\Controllers;

use App\Models\Services;
use Illuminate\Http\Request;
use DB;
use Auth;


class ServiceController extends Controller
{
    // index
    public function index()
    {
        $service = DB::table('services')->get();
        return view('services.service',compact('service'));
    }

    public function addNew()
    {
        return view('services.add_new_service');
    }

    // save new user
    public function addNewServiceSave(Request $request)
    {
       $request->validate([
           'name'   => 'required|string|max:255',
       ]);

       $service = new Services();
       $service->name = $request->name;

       $service->save();
       //Toastr::success('Create new account successfully :)','Success');
       return redirect('services/service');
   }

// update
public function edit_service($id)
{

    $data = DB::table('services')->where('id',$id)->get();
    return view('services.editservice',compact('data'));
}

public function update(Request $request)
{
    $id           = $request->id;
    $name     = $request->name;

    $update = [

        'id'           => $id,
        'name'         => $name,
    ];
    Services::where('id',$request->id)->update($update);
    //Toastr::success('User updated successfully :)','Success');
    return redirect('services/service');
}

// delete
   public function delete($id)
   {
       $delete = Services::find($id);
       $delete->delete();
       //Toastr::success('User deleted successfully :)','Success');
       return redirect('services/service');
   }
}

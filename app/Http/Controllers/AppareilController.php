<?php

namespace App\Http\Controllers;

use App\Models\Appareil;
use App\Models\Services;
use Illuminate\Http\Request;
use DB;

class AppareilController extends Controller
{
    // index
    public function index()
    {
        $user = DB::table('appareils')->get();
        return view('appareils.appareil',compact('user'));
    }

    public function addNew()
    {
        $services = Services::all();

        return view('appareils.add_new_appareil')->with('services', $services);;
    }

    // save new user
    public function addNewAppareilSave(Request $request)
    {
       $request->validate([
           'name'               => 'required|string|max:255',
           'alimentation'       => 'required|string|max:255',
           'nbre'               => 'required',
           'PN'                 => 'required',
           'cosp'               => 'required',
           'PAp'                => 'required',
           'EC'                 => 'required',
           'appareil_service'   => 'required',
       ]);

       $appareil = new Appareil();
       $appareil->name = $request->name;
       $appareil->alimentation = $request->alimentation;
       $appareil->nbre = $request->nbre;
       $appareil->PN = $request->PN;
       $appareil->cosp = $request->cosp;
       $appareil->PAp = $request->PAp;
       $appareil->EC = $request->EC;
       $appareil->appareil_service = $request->appareil_service;

       $appareil->save();
       //Toastr::success('Create new account successfully :)','Success');
       return redirect('appareils/appareil');
   }

   // update
    public function edit_appareil($id)
    {
        $data = DB::table('appareils')->where('id',$id)->get();
        $services = Services::all();

        return view('appareils.editappareil',compact('data', 'services'));
    }

    public function update_appareil(Request $request)
    {
        $id                   = $request->id;
        $nbre                 = $request->nbre;
    
        $update = [

            'id'                => $id,
            'nbre'              => $nbre,

        ];
        Appareil::where('id',$request->id)->update($update);
        //Toastr::success('User updated successfully :)','Success');
        return redirect('taches/tache');
    }




    // delete
   public function delete($id)
   {
       $delete = Appareil::find($id);
       $delete->delete();
       //Toastr::success('User deleted successfully :)','Success');
       return redirect('appareils/appareil');
   }
}

<?php

namespace App\Http\Controllers;

use App\Models\Appareil;
use App\Models\Services;
use App\Models\Tache;
use App\Models\User;
use Illuminate\Http\Request;
use DB;

class TacheController extends Controller
{
    // index
    public function index()
    {
        $tache = Tache::all();
        //$apps = Appareil::all();
        //$apps = DB::table('appareils')->get();

        return view('taches.tache',compact('tache'));
    }

    public function addNew()
    {
        $services = Services::all();
        $users = User::all();

        //$apps = Appareil::get()->pluck('name');

    //$apps = Appareil::all()->pluck('name', 'id');
        $apps = Appareil::all();
        return view('taches.add_new_tache', compact('services', 'apps', 'users'));
    }

    // save new user
    public function addNewTacheSave(Request $request)
    {

       $request->validate([
           'name'          => 'required|string|max:255',
           'date'          => 'required',
           'h_debut'       => 'required',
           'h_fin'         => 'required',
           'duree'         => 'required',
           'tache_service' => 'required',
           'tache_appareil' => 'required',
           'tache_user' => 'required',
       ]);

       $input = $request->all();

       $tache = new Tache();
       $tache->name = $request->name;
       $tache->date = $request->date;
       $tache->h_debut = $request->h_debut;
       $tache->h_fin = $request->h_fin;
       $tache->duree = $request->duree;
       $tache->tache_service = $request->tache_service;
       $tache->tache_user = $request->tache_user;

       $tache->save();
       //$apps = Appareil::get()->pluck('name', 'id');
       //$roles = $request->input('roles') ? $request->input('roles') : [];
       $tache->appareils()->attach($input['tache_appareil']);

       return redirect('taches/tache');
   }

   // update
   public function edit_tache($id)
   {
       $data = DB::table('taches')->where('id',$id)->get();
       $services = Services::all();
       $apps = Appareil::all();
       return view('taches.edittache',compact('data', 'services','apps'));
   }

   public function update_tache(Request $request)
   {

      // $tache->appareils()->sync($request->tache_appareil);
        //$tache = new Tache();


       $id             = $request->id;
       $name           = $request->name;
       $date           = $request->date;
       $h_debut        = $request->h_debut;
       $h_fin          = $request->h_fin;
       $tache_service  = $request->tache_service;
       $tache_appareil  = $request->tache_appareil;

       $update = [

           'id'            => $id,
           'name'          => $name,
           'date'          => $date,
           'h_debut'       => $h_debut,
           'h_fin'         => $h_fin,
           'tache_service' => $tache_service,

       ];




         $tache = Tache::find($id);
         $tache->appareils()->detach();

         Tache::where('id',$request->id)->update($update);

        $tache->appareils()->attach($tache_appareil);
       //Toastr::success('User updated successfully :)','Success');
       return redirect('taches/tache');
   }

   // delete
   public function delete($id)
   {
       $delete = Tache::find($id);
       $delete->delete();
       //Toastr::success('User deleted successfully :)','Success');
       return redirect('taches/tache');
   }



}

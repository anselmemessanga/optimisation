<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Optimisation',
            'email' => 'optimisation@gmail.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'Alain',
            'email' => 'alain@gmail.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'Brunelle',
            'email' => 'brunelle@gmail.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'Fabrice',
            'email' => 'fabrice@gmail.com',
            'password' => Hash::make('password')
        ]);

        User::create([
            'name' => 'Divin',
            'email' => 'divin@gmail.com',
            'password' => Hash::make('password')
        ]);
    }
}

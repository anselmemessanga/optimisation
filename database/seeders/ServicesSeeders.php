<?php

namespace Database\Seeders;

use App\Models\Services;
use Illuminate\Database\Seeder;

class ServicesSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Services::create([
            'name' => 'IMAGERIE',
        ]);

        Services::create([
            'name' => 'OPHTALMOGIE',
        ]);

        Services::create([
            'name' => 'LABORATOIRE',
        ]);

        Services::create([
            'name' => 'BLOC OPERATOIRE',
        ]);

    }
}

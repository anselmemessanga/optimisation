<?php

namespace Database\Seeders;

use App\Models\Appareil;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppareilsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Appareil::truncate();

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Eclairage',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN' => 0.36,
            'cosp' => 0.6,
            'PAp'=> 0.36,
            'EC' => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Climatisseur split ',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);
        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Agitateur de bain',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Aspirateur chirurgical de capacité moyenne',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Développeuse automatique de tabl',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Echographe',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Lampe inactinique',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Négatoscope',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Séchoir électrique ',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'IMAGERIE',
            'name'              => 'Ventilateur extracteur de chambre noire',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);





        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Générateur haute fréquence ',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Aspirateur chirurgical à 2 bocaux de 3 L',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Eclairage opératoire mobile',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Eclairage opératoire plafonnier',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Microscope opératoire',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => "Colonne d'anesthésie",
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Bistourie électrique ',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Stérilisateur à vapeur grande capacité',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'BLOC OPERATOIRE',
            'name'              => 'Eclairage',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);




        Appareil::create([
            'appareil_service'  => 'LABORATOIRE',
            'name'              => 'Agitateur électromagnétiqu',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPERATOIRE',
            'name'              => 'Centrifugeuse cytologie de table',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPERATOIRE',
            'name'              => 'Centrifugeuse micro hématocrite',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPERATOIRE',
            'name'              => "Distillateur d'eau",
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPERATOIRE',
            'name'              => 'Microscope binoculaire',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPERATOIRE',
            'name'              => 'Plaque chauffante',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPERATOIRE',
            'name'              => 'Spectrophotomètre',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);



        Appareil::create([
            'appareil_service'  => 'OPHTALMOGIE',
            'name'              => "Tonomètre à pulsation d'air",
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPHTALMOGIE',
            'name'              => 'Biomicroscope',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPHTALMOGIE',
            'name'              => 'Frontocomètre',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPHTALMOGIE',
            'name'              => 'Autoréfracto kérato',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);

        Appareil::create([
            'appareil_service'  => 'OPHTALMOGIE',
            'name'              => 'Pojecteur de tests ophtalmiques',
            'alimentation'      => '220V – 240V 50 Hz ',
            'nbre'              => 1,
            'PN'                => 0.36,
            'cosp'              => 0.6,
            'PAp'               => 0.36,
            'EC'                => 0.36,
        ]);


    }
}

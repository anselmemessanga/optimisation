<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppareilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appareils', function (Blueprint $table) {
            $table->id();
            $table->string('appareil_service')->nullable();
            $table->string('name')->nullable();
            $table->string('alimentation')->nullable();
            $table->integer('nbre')->nullable();
            $table->double('PN')->nullable();
            $table->double('cosp')->nullable();
            $table->double('PAp')->nullable();
            $table->double('EC')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appareils');
    }
}

 <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html"> <img alt="image" src="{{URL::to('assets/img/logo.png')}}" class="header-logo" /> <span
                class="logo-name">Optimisation</span>
            </a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li class="dropdown active">
              <a href="{{URL::to('dashboard')}}" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
            </li>
            <li class="dropdown">
                <a href="{{URL::to('taches/tache')}}" class="nav-link">
                    <i data-feather="briefcase"></i><span>Tâches</span>
                </a>
            </li>
            <li class="dropdown">
                <a href="{{URL::to('services/service')}}" class="nav-link">
                    <i data-feather="briefcase"></i><span>Services</span>
                </a>
            </li>
            <li class="dropdown">
                <a href="{{URL::to('appareils/appareil')}}" class="nav-link">
                    <i data-feather="briefcase"></i><span>Appareils utilisés</span>
                </a>
            </li>
            <li class="dropdown">
                <a href="{{URL::to('user/management')}}" class="nav-link">
                    <i data-feather="briefcase"></i><span>Listes utilisateurs</span>
                </a>
            </li>

          </ul>
        </aside>
      </div>
